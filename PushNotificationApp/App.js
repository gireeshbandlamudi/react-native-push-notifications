import React, { Component } from 'react';

import { createStackNavigator, createAppContainer } from 'react-navigation';
import ScreenOne from './src/screenOne';
import ScreenTwo from './src/screenTwo';

const RootStack = createStackNavigator(
  {
    ScreenOne: {screen: ScreenOne},
    ScreenTwo: {screen: ScreenTwo},
  },
  {
    initialRouteName: 'ScreenOne',
  }
);

const AppContainer = createAppContainer(RootStack);

export default class App extends Component {
  render() {
    return <AppContainer />;
  }
}
