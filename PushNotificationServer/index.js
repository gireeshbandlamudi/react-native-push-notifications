var admin = require("firebase-admin");

var serviceAccount = require("./tataforum-6cbec-firebase-adminsdk-bmjca-0f7f0f7c68.json");

var payload = {
    notification: {
        title: "This is a Notification",
        body: "This is the body of the notification message."
    }
};

var options = {
    priority: "high",
    timeToLive: 60 * 60 * 24
};

var registrationToken = "dfVNVgoVVxE:APA91bH5Rtk-SV-em40Y2CyR4On57ViUEg4k8qKCRpLxHl2EYFzkmDX5JkA2WibdID1UH2obqGA1nl-mgLxeSErd2xoOwvzcE4su7_Jpx5y7bd4_Kpz01z5xP-ZxRjwHVAAkMU9lmRrd";

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://tataforum-6cbec.firebaseio.com"
});

admin.messaging().sendToDevice(registrationToken, payload, options)
    .then(function (response) {
        console.log("Successfully sent message:", response);
    })
    .catch(function (error) {
        console.log("Error sending message:", error);
    });